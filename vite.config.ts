import { defineConfig } from 'vite'
import { VitePWA } from 'vite-plugin-pwa'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(),
  VitePWA({
    registerType: 'autoUpdate', injectRegister: 'auto',
    includeAssets: ['favicon.ico', 'apple-touch-icon.png', 'vite.svg'],
    manifest: {
      name: 'Store 24',
      short_name: 'Store24',
      description: 'CRUD for a convenience store',
      theme_color: '#0d909e',
      icons: [
        {
          src: 'favicon/android-chrome-192x192.png',
          sizes: '192x192',
          type: 'image/png'
        },
        {
          src: 'favicon/android-chrome-512x512.png',
          sizes: '512x512',
          type: 'image/png'
        }
      ]
    }
  })],
})
