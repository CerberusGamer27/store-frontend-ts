import { FormType } from "../components/ProductForm/ProductForm.types";
import axios from "./axios";

export const getProducts = () => {
  return axios.get('/api/products');
}

export const getProductById = (id: string) => {
  return axios.get(`/api/product/${id}`);
}

export const createProduct = (data: any) => {
  return axios.post('/api/product', data);
}

export const updateProduct = (id: string, data: any) => {
  return axios.patch(`/api/product/${id}`, data);
}

export const deleteProduct = (id: string) => {
  return axios.delete(`/api/product/${id}`)
}