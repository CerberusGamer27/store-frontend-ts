import { SetStateAction, Dispatch } from "react"
import { ProductType } from "../../api/ProductType"

export type ProductTableType = {
    data: ProductType[]
    onOpenModal: () => void
    setIdProduct: Dispatch<SetStateAction<string | undefined>>
    handleDeleteProduct: (id: any) => void
}