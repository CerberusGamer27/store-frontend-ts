import React, { useState } from 'react'
import {
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableCaption,
    TableContainer,
    Button,
    ButtonGroup,
    useDisclosure,
    AlertDialog,
    AlertDialogBody,
    AlertDialogFooter,
    AlertDialogHeader,
    AlertDialogContent,
    AlertDialogOverlay,
    InputGroup,
    InputLeftElement,
    FormControl,
    FormLabel,
    Input,
    Box
} from '@chakra-ui/react'
import { ProductTableType } from './ProductTable.type'

const ProductTable = ({ data, onOpenModal, setIdProduct, handleDeleteProduct }: ProductTableType) => {

    const [productId, setProductId] = useState<string>("");


    const { isOpen, onOpen, onClose } = useDisclosure();
    const cancelRef = React.useRef<any>()


    return (
        <>

            <AlertDialog
                isOpen={isOpen}
                leastDestructiveRef={cancelRef}
                onClose={onClose}
            >
                <AlertDialogOverlay>
                    <AlertDialogContent>
                        <AlertDialogHeader fontSize='lg' fontWeight='bold'>
                            Borrar Producto
                        </AlertDialogHeader>

                        <AlertDialogBody>
                            Esta Seguro esta accion no se puede deshacer
                        </AlertDialogBody>

                        <AlertDialogFooter>
                            <Button ref={cancelRef} onClick={onClose}>
                                Cancelar
                            </Button>
                            <Button colorScheme='red' onClick={() => {
                                handleDeleteProduct(productId);
                                onClose();
                            }} ml={3}>
                                Eliminar
                            </Button>
                        </AlertDialogFooter>
                    </AlertDialogContent>
                </AlertDialogOverlay>
            </AlertDialog>

            {/* <Box overflow={"scroll"}> */}
                <TableContainer maxWidth={"1000px"} maxHeight={"500px"} m={"auto"} mt={10} overflowY={'scroll'}>
                    <Table variant='simple' size={"sm"}>
                        <TableCaption>Tabla de Productos</TableCaption>
                        <Thead>
                            <Tr>
                                <Th>Marca</Th>
                                <Th>Modelo</Th>
                                <Th>PRESENTACIÓN</Th>
                                <Th isNumeric>Precio de Compra</Th>
                                <Th isNumeric>Precio de Venta</Th>
                                <Th isNumeric>Ganancia</Th>
                                {/* <Th>Estado</Th> */}
                                <Th>Acciones</Th>
                            </Tr>
                        </Thead>
                        <Tbody>
                            {data.map((item) => (
                                <Tr key={item._id}>
                                    <Td>{item.brand}</Td>
                                    <Td>{item.model}</Td>
                                    <Td>{item.presentation}</Td>
                                    <Td isNumeric>${item.purchasePrice}</Td>
                                    <Td isNumeric>${item.salePrice}</Td>
                                    <Td isNumeric>${item.saleGain}</Td>
                                    {/* <Td>{item.status ? "Activo" : "Inactivo"}</Td> */}
                                    <Td>
                                        <ButtonGroup variant='outline' spacing='6'>
                                            <Button colorScheme='blue' onClick={() => {
                                                onOpenModal();
                                                setIdProduct(item._id);
                                            }}>Editar</Button>
                                            <Button colorScheme={"red"} onClick={() => {
                                                onOpen();
                                                setProductId(item._id);
                                            }}>Eliminar</Button>
                                        </ButtonGroup>
                                    </Td>
                                </Tr>
                            ))}
                        </Tbody>
                        {/* <Tfoot>
                    <Tr>
                    <Th>To convert</Th>
                    <Th>into</Th>
                    <Th isNumeric>multiply by</Th>
                    </Tr>
                </Tfoot> */}
                    </Table>
                </TableContainer>
            {/* </Box> */}
        </>
    )
}

export default ProductTable