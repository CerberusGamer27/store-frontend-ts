export type FormType = {
    brand: string
    model: string
    presentation: string
    purchasePrice: number
    salePrice: number
    status: boolean
}


export type ProductFormType = {
    handleGetStudents: () => void
    setIsLoading: React.Dispatch<React.SetStateAction<boolean>>
}
