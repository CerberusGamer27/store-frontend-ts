import React, { useState, useEffect } from 'react'
import {
    Box,
    Input,
    FormLabel,
    FormControl,
    FormErrorMessage,
    FormHelperText,
    SimpleGrid,
    Button,
    ButtonGroup,
    Heading
} from "@chakra-ui/react"
import { useForm } from 'react-hook-form';
import { FormType } from './ProductForm.types';
import { createProduct } from '../../api/productsApi';
import { toast } from 'react-toastify';


const ProductForm = ({ setIsLoading, handleGetProducts }: any) => {

    const { register, handleSubmit, reset, setValue } = useForm<FormType>();
    const [errorMessages, setErrorMessage] = useState<any>();

    // Submit Form
    const submit = async (data: any, event: any) => {
        setIsLoading(true);
        let formData = new FormData();
        formData.append("brand", data.brand);
        formData.append("model", data.model);
        formData.append("presentation",data.presentation);
        formData.append("purchasePrice", data.purchasePrice);
        formData.append("salePrice", data.salePrice);
        formData.append("status", 'true');

        try {
            const response = await createProduct(formData);
            console.log(response.data);
            if (response.data.code === 201) {
                handleGetProducts();
                setIsLoading(false);
                event.target.reset();
                toast.success(response.data.message);
            }
        } catch (error) {
            setIsLoading(false);
            toast.error("Error al guardar la informacion");
        }

    }

    return (
        <Box maxWidth={"1000px"} m={"auto"} borderWidth={1} p={4} boxShadow={"lg"}>
            <Heading py={5} size={'md'}>Formulario para Ingreso de Productos</Heading>
            <form onSubmit={handleSubmit(submit)} encType='multipart/data'>
                <SimpleGrid columns={{ sm: 1, md: 2 }} spacing={"15px"}>
                    <FormControl isRequired>
                        <FormLabel>Marca del Producto</FormLabel>
                        <Input {...register("brand")} placeholder='Marca' type={"text"} name={'brand'} />
                    </FormControl>
                    <FormControl isRequired>
                        <FormLabel>Nombre del Producto</FormLabel>
                        <Input {...register("model")} placeholder='Modelo' type={"text"} name={'model'} />
                    </FormControl>
                    <FormControl>
                        <FormLabel>Presentación del Producto</FormLabel>
                        <Input {...register("presentation")} placeholder='Presentación' type={"text"} name={'presentation'} />
                    </FormControl>
                    <FormControl isRequired>
                        <FormLabel>Precio de Compra 📉</FormLabel>
                        <Input {...register("purchasePrice")} placeholder='$' type={"number"}  step={0.01} min={0} name={'purchasePrice'} />
                    </FormControl>
                    <FormControl isRequired>
                        <FormLabel>Precio de Venta 📈</FormLabel>
                        <Input {...register("salePrice")} placeholder='$' type={"number"}  step={0.01} min={0} name={'salePrice'} />
                    </FormControl>
                </SimpleGrid>
                <ButtonGroup variant={"outline"}>
                    <Button type="submit" colorScheme={"blue"} mt={"5"}>
                        Agregar Producto
                    </Button>
                    <Button type="reset" colorScheme={"orange"} mt={"5"}>
                        Limpiar
                    </Button>
                </ButtonGroup>
            </form>
        </Box>
    )
}

export default ProductForm