import React, { useEffect, useState } from 'react'
import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    Button,
    FormControl,
    FormLabel,
    Input,
    SimpleGrid,
} from '@chakra-ui/react'
import { ProductModalType } from './ProductModal.type'
import { getProductById, updateProduct } from '../../api/productsApi'
import { useForm } from 'react-hook-form';
import { FormType } from '../ProductForm/ProductForm.types';
import { toast } from "react-toastify";
import SkeletonForm from '../SkeletonForm/SkeletonForm';

function ProductModal({ isOpen, onClose, idProduct, handleGetProducts }: ProductModalType) {
    //const { isOpen, onOpen, onClose } = useDisclosure()

    //const initialRef = React.useRef(null)
    //const finalRef = React.useRef(null)
    const [isLoading, setIsLoading] = useState(false);
    const { register, handleSubmit, reset, setValue } = useForm<FormType>();


    const handleGetStudentById = async () => {
        setIsLoading(true);
        const res = await getProductById(idProduct as string);
        if (res.data.code === 200) {
            console.log(res.data);
            reset(res.data.data);
        }
        setIsLoading(false);
    };

    const submit = async (data: any, event: any) => {
        let formData = new FormData();
        formData.append("brand", data.brand);
        formData.append("model", data.model);
        formData.append("presentation", data.presentation);
        formData.append("purchasePrice", data.purchasePrice);
        formData.append("salePrice", data.salePrice);
        formData.append("status", 'true');

        try {
            const response = await updateProduct(data._id, formData);
            if (response.data.code === 200) {
                console.log(response.data);
                handleGetProducts();
                setIsLoading(false);
                event.target.reset();
                toast.success(response.data.message);
                onClose();
            }
        } catch (error) {
            setIsLoading(false);
            toast.error("Error al guardar la informacion");
        }
    }


    useEffect(() => {
        if (idProduct !== undefined) {
            handleGetStudentById();
        }
    }, [idProduct]);


    return (
        <>
            {/* <Button onClick={onOpen}>Open Modal</Button>
            <Button ml={4} ref={finalRef}>
                I'll receive focus on close
            </Button> */}

            <Modal
                isOpen={isOpen}
                onClose={onClose}
                size={"xl"}
                closeOnOverlayClick={false}
                >
                <ModalOverlay />
                <ModalContent>
                    <form onSubmit={handleSubmit(submit)} encType='multipart/data'>
                        <ModalHeader>Editar Producto</ModalHeader>
                        <ModalBody pb={6}>
                            {!isLoading ? (
                                <SimpleGrid columns={{ sm: 1, md: 2 }} spacing={"15px"}>
                                    <FormControl isRequired>
                                        <FormLabel>Marca del Producto</FormLabel>
                                        <Input {...register("brand")} placeholder='Marca' type={"text"} name={'brand'} />
                                    </FormControl>
                                    <FormControl isRequired>
                                        <FormLabel>Nombre del Producto</FormLabel>
                                        <Input {...register("model")} placeholder='Modelo' type={"text"} name={'model'} />
                                    </FormControl>
                                    <FormControl>
                                        <FormLabel>Presentación del Producto</FormLabel>
                                        <Input {...register("presentation")} placeholder='Presentación' type={"text"} name={'presentation'} />
                                    </FormControl>
                                    <FormControl isRequired>
                                        <FormLabel>Precio de Compra 📉</FormLabel>
                                        <Input {...register("purchasePrice")} placeholder='$' type={"number"} step={0.01} min={0} name={'purchasePrice'} />
                                    </FormControl>
                                    <FormControl isRequired>
                                        <FormLabel>Precio de Venta 📈</FormLabel>
                                        <Input {...register("salePrice")} placeholder='$' type={"number"}  step={0.01} min={0} name={'salePrice'} />
                                    </FormControl>
                                </SimpleGrid>
                            ) : (
                                <SkeletonForm />
                            )}
                        </ModalBody>

                        <ModalFooter>
                            <Button colorScheme='blue' mr={3} type={"submit"}>
                                Guardar
                            </Button>
                            <Button onClick={() => {
                                onClose();
                                handleGetStudentById();
                                }}>Cancelar</Button>
                        </ModalFooter>
                    </form>
                </ModalContent>
            </Modal>
        </>
    )
}

export default ProductModal