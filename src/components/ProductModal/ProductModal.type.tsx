import { SetStateAction, Dispatch } from "react"

export type ProductModalType = {
    isOpen: boolean
    onClose: () => void
    idProduct: string | undefined
    handleGetProducts: () => void
}