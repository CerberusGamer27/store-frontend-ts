import React, { useState, useEffect } from 'react'
import ProductForm from '../ProductForm/ProductForm'
import {
    Box, Spinner, useDisclosure, FormControl, FormLabel, Input, Heading, SimpleGrid
} from '@chakra-ui/react'
import ProductTable from '../ProductTable/ProductTable'
import { toast, ToastContainer } from 'react-toastify'
import { ProductType } from '../../api/ProductType'
import { deleteProduct, getProducts } from '../../api/productsApi'
import ProductModal from '../ProductModal/ProductModal'

const Product = () => {
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [isGetData, setIsGetData] = useState<boolean>(false);
    const [products, setProducts] = useState<ProductType[]>([]);
    const [idProduct, setIdProduct] = useState<string | undefined>(undefined);

    const [query, setQuery] = useState<string>("");
    const keys = ["brand", "model", "presentation"]

    // For Modal
    const { isOpen, onClose, onOpen } = useDisclosure();

    //const cancelRef = React.useRef<any>()


    const handleGetProducts = async () => {
        setIsGetData(true);

        const res = await getProducts();
        if (res.data.code === 200) {
            console.log(res.data.data);
            setProducts(res.data.data);
        }
        setIsGetData(false);
    };

    const handleDeleteProduct = async (id: any) => {
        console.log(id);
        setIsGetData(true);
        const res = await deleteProduct(id);
        if (res.data.code === 200) {
            toast.success(res.data.message);
            handleGetProducts();
        }
        setIsGetData(false);
    }

    useEffect(() => {
        handleGetProducts();
    }, [])

    const search = (data: ProductType[]) => {
        return data.filter((item: any) =>
            keys.some((key) => item[key].toLowerCase().includes(query))
        );
    }
    return (
        <Box textAlign={'center'} margin="30px auto">
            {isLoading ? <div className='loading-lazy'></div> : ""}
            <ProductForm setIsLoading={setIsLoading} handleGetProducts={handleGetProducts} />

            <ProductModal isOpen={isOpen} onClose={onClose} idProduct={idProduct} handleGetProducts={handleGetProducts} />

            <Box maxWidth={"1000px"} m={"auto"} my={5} p={5}>
                <SimpleGrid column={{sm: 1, md: 1}} spacing={'15px'}>
                    <FormControl>
                        <FormLabel>Buscar un Producto</FormLabel>
                        <Input type='text' onChange={(e) => setQuery(e.target.value.toLowerCase())} />
                    </FormControl>
                </SimpleGrid>
            </Box>

            {!isGetData ? <ProductTable handleDeleteProduct={handleDeleteProduct} data={search(products)} onOpenModal={onOpen} setIdProduct={setIdProduct} /> : <Spinner size={"xl"} mt={10} />}
            <ToastContainer />
        </Box>
    )
}

export default Product