import { useState } from 'react'
import { ChakraProvider, Box, VStack, theme } from "@chakra-ui/react"
import { ColorModeSwitcher } from './components/ColorModeSwitcher'
import Product from './components/Product/Product'
import "./test.css"
import "react-toastify/dist/ReactToastify.css";



function App() {

  return (
    <ChakraProvider theme={theme}>
      <Box fontSize={'xl'}>
        <ColorModeSwitcher justifySelf={'flex-end'} />
        {/* <VStack spacing={8}>
          
        </VStack> */}
        <Product />
      </Box>
    </ChakraProvider>
  )
}

export default App
